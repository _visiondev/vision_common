import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class VisionAuthor extends StatelessWidget {
  final String? customTitle;
  final Color? titleColor;
  final Color? ownerColor;
  final CrossAxisAlignment? align;
  final double? titleSize;
  final double? ownerSize;

  const VisionAuthor({Key? key, this.titleColor, this.ownerColor, this.customTitle, this.align, this.titleSize, this.ownerSize}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: align ?? CrossAxisAlignment.center,
      children: [
        Text(
          (customTitle ?? 'Um produto de').toUpperCase(),
          style: TextStyle(
            fontSize: titleSize ?? 14,
            color: titleColor,
          ),
        ),
        Text(
          'Vision Development'.toUpperCase(),
          style: GoogleFonts.spaceMono(fontWeight: FontWeight.w800, fontSize: ownerSize ?? 20.0, color: ownerColor),
        ),
      ],
    );
  }
}
